﻿using UnityEngine;

public class Music : MonoBehaviour {

    private static bool start = true;

	// Use this for initialization
	void Awake () {
        if (!start)                         //start == false
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        start = false;
    }
}
