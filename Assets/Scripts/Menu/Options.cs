﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Options : MonoBehaviour {

    [Header("Volume")]
        public AudioMixer audioMixer;
    [Header("Resolution")]
        public Dropdown resolutionDropdown;

    Resolution[] resolutions;

    void Start()
    {
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();

        int currResolutionIndex = 0;

        for(int i=0; i<resolutions.Length; i++)     // iterate through every resolution oprion 
        {
            options.Add(resolutions[i].width + " x " + resolutions[i].height); // adds each resolution to options List array

            if (resolutions[i].width == Screen.currentResolution.width &&
               resolutions[i].height == Screen.currentResolution.height) // automatically set resolution to your default resolution
            {
                currResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);                         // adds resolutions to the dropdown (in Unity)
        resolutionDropdown.value = currResolutionIndex;                 // sets resolution to current resolution
        resolutionDropdown.RefreshShownValue();
    }

    public void SetResolution(int resolutionIndex, bool isFullScreen)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width,resolution.height,Screen.fullScreen = isFullScreen); //Sets screen resolution
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }
}
