﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour {

    public static bool gameIsPaused = false;    // static reason: PlayerMove script is also using this var.
    public GameObject pauseMenuUI;

    void Start()                            // bug fix: after returning to main menu, game remain paused after selecting play button
    {
        if (gameIsPaused)                   // if gameIsPaused == true, 
        {                                       // than Resume the game 
            Resume();                                              
            //pauseMenuUI.SetActive(false);
        }
        DontDestroyOnLoad(gameObject);      // script don't destroy after entering a new level
    }                                       // by that fact, it's not needed to add this script in every scene

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
                Resume();
            else
                Pause2();
        }
	}

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;                 // Time.timeScale - sets the game speed (normal speed is 1)
        gameIsPaused = false;
    }

    void Pause2()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
        Player.deathCalc = 0;           // resetting deathCalc to '0' (PlayerMove script's static variable)
        Destroy(gameObject);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
