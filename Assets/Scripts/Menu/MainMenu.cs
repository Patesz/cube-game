﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    // private static bool firstLoad = true;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void PlayGame()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }

	public void QuitGame(){
		Debug.Log ("QUIT!");
		Application.Quit();
	}

}
