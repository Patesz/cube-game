﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPatrol : MonoBehaviour {

	public Transform[] patrolPoints;
	public float patrolSpeed;
	public int max; 

	private int currPoint;

	// Update is called once per frame
	void Update () {

		//set position to +1 if current position and the patrol point is equal
		if (transform.position == patrolPoints [currPoint].position) {
			int rand = Random.Range (1, max);
			currPoint += rand;
		}

		//if current Point is greater or eqauls with the number of patrol points then reset value and start again
		if (currPoint >= patrolPoints.Length) {
			currPoint = 0;
		}

		//move the object 
		transform.position = Vector3.MoveTowards (transform.position, patrolPoints [currPoint].position, patrolSpeed * Time.deltaTime);

	}
}
