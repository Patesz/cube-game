﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour {

    public float patrolSpeed;
    private int currPoint;

    private float timer;
    private float timerMax;
    
    public Transform[] patrolPoints;

    private float waitTime;

    [Header("WaitTime")]
        public float waitMin;
        public float waitMax;

    [Header("RandomPatrol")]
        public int randMax;

    // Update is called once per frame
    void Update () {

        if (transform.position == patrolPoints[currPoint].position)
        {
            int rand = Random.Range(1, randMax);

            waitTime = Random.Range(waitMin, waitMax);
            if (Waited(waitTime))
            {
                currPoint+=rand;
            }
        }

		//if current Point is greater or eqauls with the number of patrol points then reset value and start again
		if (currPoint >= patrolPoints.Length) {
			currPoint = 0;
		}

		//moves the object: 1.current pos, 2.target pos, 3.speed
		transform.position = Vector3.MoveTowards (transform.position, patrolPoints [currPoint].position, patrolSpeed * Time.deltaTime);
	}

    private bool Waited(float seconds){
		timerMax = seconds;
		timer += Time.deltaTime;
		if (timer >= timerMax) {
			timer = 0;
			return true;	
		}
		return false;
	}
}
