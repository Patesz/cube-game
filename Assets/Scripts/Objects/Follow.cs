﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {

	public Transform FollowObject;
	float speed = 2.0f;
	const float distance = 0.1f;

	// Update is called once per frame
	void Update () {
		transform.LookAt(FollowObject.position);								// looks to the object we set in unity

		if ((transform.position - FollowObject.position).magnitude > distance)	// this prevent the objects to stick with each other
			transform.Translate(0.0f, 0.0f, speed*Time.deltaTime);			    // move the object
	}
}
