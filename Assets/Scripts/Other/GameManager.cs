﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static short currentLevel = 1;

	private Text text;

	void Awake(){
		text = GetComponent <Text> ();
	}

	/*void Start(){
		DontDestroyOnLoad (gameObject);
	}*/

	void Update(){
		text.text = "Deaths: " + Player.deathCalc;
	}

	public static void CompleteLevel(){
		currentLevel += 1;
        SceneManager.LoadScene(currentLevel);
	}


}
