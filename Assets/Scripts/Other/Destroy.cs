﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {
	private int lifetime = 1;

	void Start () {
		Destroy (gameObject, lifetime);
	}
}
