﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


public class Player : MonoBehaviour {
    [Header("Movement")]                // adds highlighted header text in unity in script setup
        public float moveSpeed = 15;
        public float maxSpeed = 8;

    [Header("DeathEffect")]
        public GameObject CollisionEffect;

    [Header("Audio")]
        public AudioClip[] soundArray;

    private Vector3 input, respawn;
    private bool isWait = true;

	public static int deathCalc = 0;

	void Start () {
		respawn = transform.position;	// saves start respawn position location
	}

    void FixedUpdate(){
        if (!Pause.gameIsPaused && isWait == true)            // bug fix: prevent to get user control inputs while the game is paused
        {
            input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

            if (GetComponent<Rigidbody>().velocity.magnitude < maxSpeed)
                GetComponent<Rigidbody>().AddForce(input * moveSpeed);

            if (transform.position.y < -3 || Input.GetKeyDown("k")) // if player's y coord is less then -3 or user key input equals with k than player dies     
                Die();

        }
    }

	//if the collided object's tag is "(object name)" than Player die 
	void OnCollisionEnter(Collision other)		//OnCollisionEnter: triggered once per collision
	{                                           //OnCollisionStay: triggered every frame while hitting object
        if (other.transform.tag == "Enemy")
        {
            PlaySound(0,0.1f);
            Die();
        }
	}

	void OnTriggerEnter(Collider other){

        if (other.transform.tag == "Objective")
        {
            PlaySound(0,0.15f) ;
            GameManager.CompleteLevel();
        }
	}

    void Die()
    {
        Instantiate(CollisionEffect, transform.position, Quaternion.Euler(270, 0, 0)); //Starts the effect, at the current position, with no rotation
        transform.position = respawn;                                                 //respawn to the default respawn point
        deathCalc++;
        //isWait = false;
        //BlockMovement(500);
        //StartCoroutine("WaitSeconds");
        input = Vector3.zero;
        GetComponent<Rigidbody>().AddForce
    }

    IEnumerator WaitSeconds()
    {
        gameObject.SetActiveRecursively(false);
        yield return new WaitForSeconds(2);
        Debug.Log("Waited two sec.");
        gameObject.SetActive(true);
    }

    void PlaySound(int clip, float vol)
    {
        GetComponent<AudioSource>().clip = soundArray[clip];
        GetComponent<AudioSource>().volume = vol;
        GetComponent<AudioSource>().Play();
    }

    private float timePassed = 0;
    private void BlockMovement(float timeToWait)
    {
        //WaitForSeconds(timeToWait);
        while (timePassed < timeToWait)
        {
            timePassed += Time.deltaTime;
            //Debug.Log(timePassed);
        }
        timePassed = 0;
        isWait = true;
    }

}
