﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSpawn : MonoBehaviour {

	public GameObject spawnObj;

	void Start () {
		spawnObj.SetActive(false);
	}

	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "Trigger")
			TriggerOpen ();
	}

	void TriggerOpen(){
		spawnObj.SetActive (true);
	}
}
