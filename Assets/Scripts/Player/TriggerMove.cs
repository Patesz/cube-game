﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMove : MonoBehaviour {

	public GameObject moveObj;

	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "Trigger")
			TriggerOpen ();
	}

	void TriggerOpen(){
		moveObj.SetActive (false);
	}
}
